/* 
 * aturan ketidaksetaraan segitiga
 * Sisi Terpanjang: Panjang sisi segitiga tidak boleh lebih besar atau sama dengan jumlah panjang dua sisi yang tersisa.
 * a < b + c
 * b < a + c
 * c < a + b
*/

import java.util.Scanner;

// Custom exception class
class InvalidTriangleException extends Exception {

    // Constructor to initialize the exception message
    public InvalidTriangleException(String message) {
        super(message);
    }
}

public class Task1 {
    public static void main(String[] args) {
        double side1, side2, side3;

        System.out.println("ENTER SIDES OF THE TRIANGLE");
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.print("Side 1: ");
            side1 = keyboard.nextDouble();

            System.out.print("Side 2: ");
            side2 = keyboard.nextDouble();

            System.out.print("Side 3: ");
            side3 = keyboard.nextDouble();

            // Validate if the sides can form a valid triangle
            validateTriangle(side1, side2, side3);

            System.out.println("Triangle with sides " + side1 + ", " + side2 + ", " + side3 + " is valid.");

        } catch (InvalidTriangleException e) {
            System.out.println("InvalidTriangleException: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
        }
    }

    // Method to validate if the sides can form a valid triangle
    private static void validateTriangle(double side1, double side2, double side3) throws InvalidTriangleException {
        if (side1 + side2 <= side3 || side1 + side3 <= side2 || side2 + side3 <= side1) {
            throw new InvalidTriangleException("Invalid sides for a triangle");
        }
    }
}
