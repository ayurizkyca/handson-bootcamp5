/*
 * Menggunakan InputMismatchException untuk mengecek apakah data input yang dimasukkan
 * berupa angka atau tidak 
*/
import java.util.InputMismatchException;
import java.util.Scanner;

public class Task2 {
    
    public static void main(String[] args) {
        int choiceNumber;
        double base, height, hypotenuse;

        System.out.println("CALCULATING AREA AND PERIMETER OF A RIGHT ANGLE TRIANGLE");
        System.out.println("1. Calculate Perimeter");
        System.out.println("2. Calculate Area");
        System.out.print("Choose Number : ");

        try (Scanner keyboard = new Scanner(System.in)) {
            choiceNumber = keyboard.nextInt();

            switch (choiceNumber) {
                case 1:
                    System.out.println("Calculating Perimeter, Enter Values (in meters)");
                    System.out.print("Base: ");
                    base = keyboard.nextInt();
                    System.out.print("Height: ");
                    height = keyboard.nextInt();
                    System.out.print("Hypotenuse: ");
                    hypotenuse = keyboard.nextInt();
                    System.out.print("Triangle Perimeter: " + perimeter(base, hypotenuse, height) + " meters");
                    break;

                case 2:
                    System.out.println("Calculating Area, Enter Values (in meters)");
                    System.out.print("Base: ");
                    base = keyboard.nextInt();
                    System.out.print("Height: ");
                    height = keyboard.nextInt();
                    System.out.print("Triangle Area: " + area(base, height) + " square meters");
                    break;

                default:
                    System.out.println("Please enter a valid number");
                    break;
            }
        } catch (InputMismatchException e) {
            System.out.println("Sorry, Please input a number");
        }
    }

    static double perimeter(double base, double hypotenuse, double height) {
        return base + hypotenuse + height;
    }

    static double area(double base, double height) {
        return 0.5 * base * height;
    }
}

